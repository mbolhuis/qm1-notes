---
title: The Formalism of Quantum Mechanics
---

**Week 4, lecture 10**

!!! summary "Learning objectives"

    After following this lecture, you will be able to:
    
    - Describe quantum states in terms of the Dirac notation.
    - Apply the language of linear algebra to describe the basic operations of quantum mechanics.
    - Determine which conditions a linear operator must satisfy to represent a physical observable and what are the implications of these conditions.

# Observables and quantum states

In the previous lectures we have discussed the application of quantum mechanics to the description of various important systems, from the harmonic oscillator to the finite square well and barrier. We have presented the various solutions that the Schroedinger equation admits for these systems, and their implications both for scattering and for bound states.

Now we are going to take a step back and present a more abstract picture of quantum theory. We will discuss the **general formalism** of quantum mechanics and show how the relevant mathematical language is that of **linear algebra**, both to describe a general quantum state as well as the operations that can be carried out on them.

## Measurements in quantum mechanics

Any physical experiment is composed of two steps: **preparation** and **measurement**. The specific preparation of the experimental set-up determines the possible outcomes of the experiment, while the subsequent measurements determine the actual value of these outcomes.

This relation between preparation and measurement is dramatically different in classicial and quantum physics.

!!! example "Measurements in classical mechanics"
    For instance, assume we want to measure the acceleration of gravity by using a pendulum of length $L$. What we should do is to measure the period $T$ and then use classical mechanics to evaluate $g=4\pi^2 L/T^2$. We can verify that the results are always the same if I change the value of the pendulum mass. If my measurement apparatus is perfect, I will obtain in all cases the same exact result if I prepare my system in the same way.

!!! example "Measurements in quantum mechanics"
    Assume that one has a quantum system that can be found in two states, $\psi_1$ and $\psi_2$ (for example two of the states of the harmonic oscillator). Now one prepares identical copies of this system with the same combination of the two states, $\psi_1$ and $\psi_2$, say $\Psi= a \psi_1+b\psi_2$. Every time one carries out a measurement in these identical quantum systems, one will find a **different result** (even if my measurement apparatus is perfect). 

In other words, quantum mechanics does not allow one to make predictions about **the outcome of individual measurements**, only about the statistical properties of a large number of measurements of the same quantum system. 

These two steps, first the preparation of the experiment, and second the measurement of some specific property of the system, can be represented in quantum mechanics by what are called the **state of the system** and the **physical observable** respectively. In other words, the **state of the system** provides a complete description of the set of probabilities associated to all conceivable measurements, while the set of **observables** associated to a quantum system are all the dynamical variables that can be measured, such as energy, position, or angular momentum.

In this lecture we present which are the mathematical objects which in quantum theory describe the **state of a system** and the **physical observables**, and what are the mathematical relations that these must obey. 

## The quantum state

Up to now we have emphasized that the state of a quantum system is completely characterized by its wavefunction, $\Psi(x)$. We would like now to talk about **quantum states** in a rather more abstract way, as elements of a special **vector space** governed by the familiar rules of linear algebra. 

!!! recall
    You are already familiar with vector spaces in Cartesian coordinates. For example, in three dimensions ($\mathbb{R}^3$) we can express any vector $\vec{v}$ as a linear superposition of three basis vectors, $\vec{v}=a_1 \vec{v}_1+a_2 \vec{v}_2 + a_3 \vec{v}_3$ where $\vec{v}_1, \vec{v}_2, \vec{v}_3$ are orthonormal. You can remind ourselves of the basic concepts of vector spaces [here](https://mathforquantum.quantumtinkerer.tudelft.nl/3_vector_spaces/).

As we will show, this language reveals the full complexity of the **formalism of quantum mechanics** and makes possible achieving a deeper understanding of the implications of quantum theory.

In general, we denote a quantum state by the $|\psi\rangle$ symbol, which is known as a **ket**. A ket is an element of an abstract complex vector space called the **Hilbert space** $\mathcal{H}$. For this reason we will also denote a quantum state by **state vector**.

!!! info "Dimensionality of the Hilbert space"
    In regular vector spaces, its dimensions are defined by the number of orthogonal basis vectors that are required to span the space, that it, to represent any arbitrary member of this space via a linear combination. For the same reason, the dimension of a Hilbert vector space $\mathcal{H}$ is defined by the **minimal number of independent state vectors** that are required to represent an arbitrary member of this space via a linear combination.

The concept of the dimensionality of the Hilbert space is subtle so let's illustrate it with two examples, corresponding respectively to Hilbert spaces of finite and infinite dimensions.

!!! example
    The electron spin can only be found in two states: pointing up ($|\uparrow \rangle$) or pointing down ($|\downarrow\rangle$). This means that the most general element of this Hilbert space can be expressed by the linear combination of this two states: $$|\psi\rangle=a|\uparrow \rangle + b|\downarrow \rangle$$. Since with only two state vectors we span the whole space (that is, its basis contains two elements) we conclude that the electron spin has associated a Hilbert space of **dimension 2**.

!!! example
    A free particle can be found anywhere in space $x$. If I denote the basis kets as $|x \rangle$, then to describe a general quantum state I will need an infinite number of them, since all positions are allowed. In this case, the most general state $|\psi\rangle$ is written as $$ |\psi\rangle = \int_{-\infty}^\infty dx \,\psi(x) |x \rangle$$ where $ \psi(x)$ is the standard wave function. Since I need an infinite number of basis vectors, we conclude that the free particle has associated a Hilbert space of **infinite dimensionality**. Note that in such cases the linear superposition is expressed in terms of an **integral** rather than a sum.

Furthermore, be careful, since the dimensionality of the Hilbert space is in general not the same as the physical (spatial) dimensions of the system under consideration.

!!! warning
    You should not confuse the dimensionality of the Hilbert space with that of the spatial dimensions of your quantum system. For example, the free particle moves in a _one dimensional physical space_ but its state vectors belong to an _infinite dimensional_ Hilbert space. 
  
In this vector space, the **observables** $Q$ will be represented by **operators** that we will denote as $\hat{Q}$ and that correspond to linear transformations acting on the state vectors and mapping them into other elements of the vector space, $\hat{Q}|\psi_1\rangle=|\psi_2\rangle$ where in general $|\psi_1\rangle \ne |\psi_2\rangle$. 

This implies that we will be able to represent physical actions applied to a quantum system (such as the time evolution or the measurement of some observable like the energy or the position) in terms of linear operators acting on the state vectors of the system.

Having the wavefunctions belonging to a vector space and with physical observables represented by operators (linear transformations) in this space, it is clear that the mathematical language relevant to describe quantum mechanics will be **linear algebra**. 

!!! summary "In summary"
    The **formalism of quantum mechanics** is built upon two fundamental concepts:

    - The state of a quantum system is completely specified by its **state vector** $|\Psi \rangle$, which is an element of an abstract complex vector space known as the **Hilbert space** $\mathscr{H}$, $|\Psi \rangle \in \mathscr{H}$.

    - All physical information about a given quantum state is encapsulated in its state vector $|\Psi \rangle$.
    
    - These state vectors are modified by **linear operators** that act upon them (and transform them into other elements of the same Hilbert space) and that determine the outcome of measurements on the system.

Since, in quantum mechanics, state vectors are members of the Hilbert vector space, operations among them will follow the standard rules of linear algebra. 

For instance, if we have two state vectors belonging to the same Hilbert space, $|\phi\rangle, |\psi\rangle \in \mathscr{H} $, the sum of the two state vectors $|\phi\rangle + |\psi\rangle = |\rho\rangle$ must also be an element of the same Hilbert space, $|\rho\rangle \in \mathscr{H} $. 

Likewise, linear algebra tells us that the following relations should hold between elements of a Hilbert space:

- *Commutative property*: $|\phi\rangle + |\psi\rangle = |\psi\rangle + |\phi\rangle$
- *Associative property*: $|\phi\rangle + (|\psi \rangle + |\rho \rangle) = (|\phi\rangle + (|\psi \rangle ) + |rho \rangle$
- *Multiplication by a scalar*: the product of a vector $|\phi\rangle$ with a complex quantity $c\in \mathbb{C}$ results in another Hilbert space vector: $c|\phi\rangle \equiv |c\phi\rangle$
- *Distributive property*: $c( |\phi \rangle + |\psi \rangle)= c|\phi\rangle + c|\psi \rangle; ~~c\in \mathbb{C}$

### Representations of the state vectors

There exist several possible representations of the state vectors, depending both on the dimensionality of the Hilbert space (finite or infinite) as well as on the choice of basis. Recall from linear algebra that the same vector can be expressed in terms of different bases.

Up to know we have been dealing mostly with Hilbert spaces of infinite dimensions. For example, a particle moving in one dimension can be found in any possible position $x$. So here we need a a function to characterise this quantum state: this is nothing but the wavefunction $\psi(x)$. We call this representation the **position space representation**. 



- Position space representation: $\langle a|b \rangle \equiv \int_{-\infty}^{+\infty} \psi^*_a(x) \psi_b(x) dx$
- Momentum space representation: $\langle a|b \rangle \equiv \int_{-\infty}^{+\infty} \phi^*_a(k) \phi_b(k) dk$
- Eigenstate representation: 
$$\langle a|b \rangle = \begin{pmatrix}a^*_1 & a^*_2 & a^*_3 & ... & a^*_n\end{pmatrix} \begin{pmatrix}b_1\\b_2\\b_3\\ \vdots \\b_n\end{pmatrix}$$



### Inner product 

From your study of linear algebra you are also familar with the concept of _scalar_ or _inner_ product between two vectors, where one can multiply two vectors between them to get a scalar (just a number). 

!!! recall
    For example, in Euclidean space $\mathbb{R}^n$ we have the _scalar product_ between two vectors $\vec{a}$ and $\vec{b}$ defined as $\vec{a} \cdot \vec{b} = \sum_{i=1}^n a_ib_i \, \in \mathbb{R}$ in terms of their components.

Likewise, in the case of the Hilbert spaces relevant to describe quantum systems, we can also define an **inner product** between two quantum states. For two state vectors $|a\rangle$ and $|b\rangle$, we denote their inner product as $\langle a|b \rangle \in \mathbb{C}$ in what is called a **braket**. 

Note that since the Hilbert space is a complex vector space, the inner product between two state vectors will be in general a complex number. Here $\langle a|$ is denoted for obvious reasons as a **bra**, and is a member of the _dual Hilbert space_ $\mathscr{H}^*$ (more about it soon).

!!! info
    The notation whereby state vectors (and their duals) are represent as **kets** $|\alpha\rangle$ and **bras** $\langle \beta |$ while their inner product is represented by a **braket** $\langle \beta|\alpha \rangle is known as the **Dirac notation**, who first introduced it. 



In the position-space representation that we have been mostly dealing with up to now, two state vectors $|a\rangle$ and $|b\rangle$ will have associated wave functions $\psi_a(x)$ and $\psi_b(x)$. In terms of these wave functions, the inner product between the state vectors $|a\rangle$ and $|b\rangle$ is defined as

$$\langle a| b \rangle\equiv \int_{-\infty}^{+\infty} \psi^*_a(x) \psi_b(x) dx;~~{\rm Eq.}[1]$$ 

which as mentioned above it is a complex scalar. However, this is _not_ the only way in which the inner product can be evaluated, their explicit expression depends on the representation that is being used to describe our quantum system.

The **bra** can be expressed in terms of the **ket** as,

$$\langle a|=(|a\rangle)^\dagger$$

The specific way in which the inner product of two quantum states is computed depends on the specific representation:

- Position space representation: $\langle a|b \rangle \equiv \int_{-\infty}^{+\infty} \psi^*_a(x) \psi_b(x) dx$
- Momentum space representation: $\langle a|b \rangle \equiv \int_{-\infty}^{+\infty} \phi^*_a(k) \phi_b(k) dk$
- Eigenstate representation: 
$$\langle a|b \rangle = \begin{pmatrix}a^*_1 & a^*_2 & a^*_3 & ... & a^*_n\end{pmatrix} \begin{pmatrix}b_1\\b_2\\b_3\\ \vdots \\b_n\end{pmatrix}$$

An important property of the inner product of two quantum states $|a\rangle$ and $|b\rangle$ is that it is the same when computed in different representations.

!!! Example
    Let's show how the inner product is the same in the position and eigenstates bases representations.

    - Position space representation:

    $$\Psi_a(x)=\sum_n c_{na} \psi_n(x);~~\Psi_b(x)=\sum_m c_{mb} \psi_m(x)$$

    $$\langle a|b \rangle=\int_{-\infty}^{\infty}\Psi^*_a(x)\Psi_b(x)dx=$$
    $$=\int_{-\infty}^{\infty}\sum_n (c^*_{na} \psi^*_n(x))(\sum_m c_{mb} \psi_m(x))dx=$$
    $$=\sum_n \sum_m c^*_{na} c_{mb}\int_{-\infty}^{\infty}\psi^*_n(x)\psi_m(x)=\sum_n c^*_{na}c_{nb}$$

    - Eigenstates representation:

    $$|a\rangle = \begin{pmatrix}c_{1a} & c_{2a} & c_{3a} & ... & c_{na}\end{pmatrix};~~|b\rangle =$$
    $$\begin{pmatrix}c_{1b}\\c_{2b}\\c_{3b}\\ \vdots \\c_{nb}\end{pmatrix}$$
    
    $$\langle a|b \rangle = \begin{pmatrix}c^*_{1a} & c^*_{2a} & c^*_{3a} & ... & c^*_{na}\end{pmatrix} \begin{pmatrix}c_{1b}\\c_{2b}\\c_{3b}\\ \vdots \\c_{nb}\end{pmatrix}=$$
    $$=\sum_n c^*_{na}c_{nb}$$

From Eq.[1] we see that $\langle a|b \rangle^*=\langle a|b \rangle$.

>**Proof:**
    $$\langle a|b \rangle=\int_{-\infty}^{\infty}\Psi^*_a{x}\Psi_b{x}dx$$
    $$(\langle a|b \rangle)^*=(\int_{-\infty}^{\infty}\Psi^*_a{x}\Psi_b{x}dx)^*=$$
    $$=\int_{-\infty}^{\infty}\Psi_a{x}\Psi^*_b{x}dx=\langle b|a \rangle$$

Additionally, the inner product of a quantum state $|\Psi> \equiv |a \rangle$ with itself is $\mathbb{R}{>0}$:
    
>**Proof:**
    $$\langle a|a \rangle=\int_{-\infty}^{\infty}\Psi^*_a{x}\Psi_a{x}dx=\int_{-\infty}^{\infty}|\Psi_a{x}|^2dx \mathbb{R}_{>0}$$


As we saw in a previous lecture, the physical interpretation of the wavefunctions requires a normalized wavefunction, that is

$$\int_{-\infty}^{\infty}\Psi^*_a{x}\Psi_a{x}dx=1$$

So, we can use the **inner product** to normalize quantum states. For instance if $|a\rangle$ is a quantum state not normalized, the normalized sate $|\tilde a\rangle$ will be given by

$$|\tilde a\rangle = \frac{1}{\sqrt{\langle a|a\rangle}}|a\rangle$$

>**Proof:**
    $$\langle \tilde a|\tilde a\rangle = (\frac{1}{\sqrt{\langle a|a\rangle}})^2\langle a|a\rangle=1$$

As you have learned in your linear algebra course, the same vector can be represented in terms of different bases. 

!!! example

    Let's take a vector in a 2-dim space,

    $$\vec{v}=(2,1)$$

    If the basis is $\vec{i}=(1,0)$ and $\vec{j}=(0,1)$ then we can express this vector as a function of its bases elements: $\vec{v}=2\vec{i}+\vec{j}$

    But now if the bases vectors are $\vec{i'}=(\frac{1}{\sqrt{2}},\frac{1}{\sqrt{2}})$ and $\vec{j'}=(\frac{1}{\sqrt{2}},-\frac{1}{\sqrt{2}})$ you can show that the same vector $\vec{v}$ now can be express as, 

    $$\vec{v}=\frac{3}{\sqrt{2}}\vec{i'}+\frac{1}{\sqrt{2}}\vec{j'}$$

    So, we can express the same vector in terms of two different bases.

The same is true for quantum states, and they can be expressed in terms of different basis, which we call **representations**.

!!! example

    As we saw in the case of the free particle the position space and momentum space representations are related to each other via Fourier transform:

    $$\phi(k)=\frac{1}{\sqrt{2\pi}}\int_{-\infty}^{+\infty}\psi(x)e^{-ikx}dx$$

In general, we can always transform between one representation/basis to another representation/basis: $$\phi(k)\rightarrow \psi(x)$$ or $$\phi(k)\leftarrow \psi(x)$$.

Another possible representation of the quantum state $|\psi(x)\rangle$ is in terms of its linear expansion in eigenvectors of a given operator, such as the Hamiltonian:

$$|\psi(x)\rangle=\sum_n c_n |\psi_n(x)\rangle $$

in this case the system is especified by the vector $$\vec{c}=\begin{pmatrix}c_1\\c_2\\c_3\\ \vdots \\c_n\end{pmatrix}$$

Since the Hilbert space is a complex vector space, the components $c_n$ will be complex numbers. As for any vector space, the values of these components depend on the specific choice of basis.

If the number of the eigenvectors $\psi_n$ is $\infty$ then the dimensions of this vector $\vec{c}$ is also $\infty$.




## Operators

In quantum mechanics, for each observable $Q$ we have associated a corresponding **operator** $\hat{Q}$. For example:

- $Q = p_{x}$ $\rightarrow$ $\hat{Q}=-i\hbar\frac{d}{dx}$ (**the momentum operator**)
- $Q = x$ $\rightarrow$ $\hat{Q}=x$ (**the position operator**)
- $Q = E$ $\rightarrow$ $\hat{Q}=-\frac{\hbar^2}{2m}\frac{\partial^2}{\partial x^2}+V(x)$ (**the energy operator**)

So far we have been working with different types of **quantum states**, all of them characterizaed by a SE which is an **eigenvalue equation**:

$$\hat{H}\psi_n(x)=E_n\psi_n(x)$$

- $\hat{H}$ the energy operator
- $\psi_n(x)$ are the eigenvectors
- $E_n$ are the eigenvalues


!!! info
    The *eigenvalue equation* of an operdator $\hat{O}$ is:

    $$\hat{O}|x\rangle=\lambda \rangle$$

    This equation has many solutions $|x_n\rangle$, which are called **eigenstates**, **eigenfunctions**, or **eigenvectors** of $\hat{O}$. For each solution $|x_n\rangle$, there is  a number $\lambda_n$, called **eigenvalue corresponding to* the eigenvector $|x_n\rangle$.

In quantum mechanics, for each observable $Q$ we have associated a similar type of **eigenvalue equation**. An operator $\hat{Q}$ in a Hilbert space ($\mathscr{H}$) with eigenvectors $\psi_n(x)$ fullfils the eigenvalue equation:

$$\hat{Q}\psi_n(x)=q_n\psi_n(x);~~Eq.[2]$$

where $q_n$ are the eigenvalues.

If there exist several different eigenvectors to the same eigenvalue, then the eigenvalue is called **degenerate**.

Solving Eq.[2] for the operators representating observables will be crucial. The resulting eigenvalues are the spectrum of the observable and some of the eigenstates are the pure states ($\langle |\rangle =1$) of the observable. 

!!! warning
    The eigenvalue problem $\hat{Q}\psi_n(x)=q_n\psi_n(x)$ has many solutions and not all of them are physically meaningful. 

For all the operators that we will discuss in this course belongs to the Hilbert space ($\mathscr{H}$). 


### Properties of linear operators in Hilbert space

- $\hat{Q} |\phi \rangle = |\psi \rangle$

- $\hat{Q}(c_1|\phi_1 \rangle + c_2 |\phi_2\rangle) = (c_1 \hat{Q}|\phi_1 \rangle + c_2 \hat{Q}|\phi_2\rangle)= (c_1 |\psi_1 \rangle + c_2|\psi_2\rangle);~~for c_1, c_2 \in $\mathbb{C}$$.

- $(\hat{Q}+\hat{P})|\phi \rangle = \hat{Q} |\phi \rangle + \hat{P} |\phi \rangle$.

- $(\hat{Q} \hat{P})|\phi \rangle= \hat{Q} (\hat{P})|\phi \rangle$

- $\hat{0}|\phi \rangle = 0, \forall \phi$

- $\hat{1}|\phi \rangle = |\phi \rangle,~~ \forall \phi$

- In general, $\hat{Q} \hat{P} |\phi \rangle \neq \hat{P} \hat{Q} |\phi \rangle,~~ \forall \phi$. We define the **commutator** between $\hat{Q}$ and $\hat{P}$ as
$$[\hat{Q}$,$\hat{P}]=\hat{Q}\hat{P}-\hat{P}\hat{Q}$$
If [\hat{Q}$,$\hat{P}]= 0 we say that the operators commute.

### Special linear operators

- *Inverse operators*: the operator inverse of a given operator $\hat{Q}$ is denoted by $\hat{Q}^{-1}$
$$\forall |\phi rangle,~~|\psi \rangle = \hat{Q} |\phi\rangle \Rightarrow |\phi\rangle = \hat{Q}^{-1} |\psi \rangle$$

- *Adjoint or Hermitian Conjugate operators*: the adjoint or Hermitian operator $\hat{Q}^{\dagger}$ is defined by
$$\langle \phi | \hat{Q}^{\dagger} |\psi \rangle = (\langle \psi | \hat{Q} |\phi \rangle)^*$$
where $|\phi \rangle$ and $|\psi \rangle$ are vectors in the Hilbert space.

- *Hermitian operators*: if the adjoint operator $\hat{Q}^{\dagger}$ is equal to the operator itself $\hat{Q}$, then we call the operator Hermitian.

!!! important
    Observables are represented by Hermitian operators.

- *Unitary operators*: If the inverse of an operator is the adjoint operator, $\hat{U}^{-1}=\hat{U}^{\dagger}$, the this operator is called a unitary operator and $\hat{U}^{\dagger}\hat{U}=\hat{U}\hat{U}^{\dagger}=\hat{1}$

- *Projection operators*: the product between a ket and a bra vector is a projection vector: $\hat{P_n}=|\psi_n \rangle \langle \psi_n|$, that projects a given state $|\phi \rangle$ onto the unit vector $|\psi_n \rangle$, 

$$\hat{P_n}|\phi \rangle = |\psi_n \rangle \langle \psi_n|\phi \rangle$$. 

If $|\phi \rangle$ is represented in the orthonotmal base $|\psi_n \rangle$, $|\phi \ rangle =\sum_n c_n |\psi_n \rangle$, then we will obtain

$$\hat{P_n}|\phi \rangle = c_n |\psi_n \rangle$$. 

By construction, projection operators are hermitian operators.





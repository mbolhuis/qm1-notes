---
title: Generalized Statistical Interpretation
---

**Week 4, lecture 12**

!!! summary "Learning objectives"

    After following this lecture you will be able:

    - To utilise the generalized statistical interpretation to predict the outcome of measurements in quantum systems.

In the previous lectures, you have learned some of the properties about statistical interpretation of the wavefunction $\psi(x)$ of a quantum system, and what are the expected outcomes when performing specific measurements in this system.

In particular, you have learned that in position space representation the wavefunction $\psi(x)$ allows us to compute the probability distribution for the particle position.

If the particle is characterized by a $\psi(x)$ then we know that $|\psi(x)|^2dx$ represents the probability $P(x)$ of finding the particle in the region of space defined by $[x,x+dx]$.

Now, let's assume we have a particle characterized by the wavefunction $\Psi(x,t)$ and we want to measure the observable $Q$ represented by the operator $\hat{Q}$. We know that if now we measure Q on the state $\Psi(x,t)$, the outcome of this measurement will be one of the eigenvalues of the Hermitian operator $\hat{Q}$. So, *what is the probability of finding a given outcome in the measurement of Q?*

To answer this we are going to state another of the axioms that define the foundations of quantum mechanics.

# Generalized Statistical Interpretation

Let's consider first the discrete case. Since the eigenvalues of the Hermitian operator $\hat{Q}$ form a complete basis, we know that we can write:

$$\Psi(x,t)=\sum_n c_n(t) \psi_n(x)$$

where the elements of the basis $\psi_n(x)$ are the eigenfunctions of $\hat{Q}$. So, *what is the probability of finding a value $q_n$ upon the measurement of Q?*

!!! important

    Upon this measurement of Q, we say that the quantum state **collapses** into the relevant eigenfunction $\psi_n(x)$.

Therefore, after the measurement of Q the quantum state will allways be a determinate state of Q.

The probability of obtaining a specific eigenvalue $q_n$ upon measuring the observable Q is

$$P_n=|c_n|^2$$

where $c_n=\langle \psi_n(x)|\psi(x) \rangle$

>*Proof*:
    We have said that if we measure $q_n$ the wavefunction of the quantum state collapses to $\psi_n$. Therefore, the overlap with the original wavefunction is 
    $$\langle \psi_n|\Psi(x,t) \rangle =\sum_m c_m(t) \langle \psi_n|\psi_m \rangle = c_n(t) $$
    And the probability is 
    $$P_n=|\langle \psi_n|\Psi(x,t) \rangle|^2=|c_n(t)|^2$$

If $|c_n(t)|^2$ is the probability of, after a measurement, finding the state in the determinate state $|\psi_n \rangle$ then what is the $\sum_n |c_n(t)|^2~~\forall n$? 

An straighforward consequence of this generalized statistical interpretation is that the total probability must be unity:

$\sum_n |c_n(t)|^2=1$

In this formalism, how we can compute the **expectation value** of our measurement Q?

Using probability theory, we know that if the result is $q_n$ with probability $P_n=|c_n(t)|^2$, then one should have that expectation value of Q,

$$\langle Q \rangle=\sum_n P_n q_n = \sum_n |c_n(t)|^2 q_n$$

>*Proof*:
    $$\langle Q \rangle=\langle \Psi | \hat{Q} \Psi \rangle = \langle \sum_m c_m(t) \psi_m| \hat{Q} \sum_n c_n(t) \psi_n \rangle = \sum_m \sum_n c_m^*(t)c_n(t) \langle \psi_m| \hat{Q}\psi_n \rangle = \sum_m \sum_n c_m^*(t)c_n(t) q_n \langle \psi_m| \psi_n \rangle = \sum_m \sum_n c_m^*(t)c_n(t) q_n \delta_mn =\sum_n |c_n(t)|^2 q_n$$



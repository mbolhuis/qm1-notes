# Learning objectives

!!! summary "Learning goals"

    After following this course you will be able:

    - to use the uncertainty principle, quantum measurement, quantum tunnelling, and zero point energy to predict how objects behave differently in quantum mechanics than in classical mechanics.

    - to calculate the probability of measurement outcomes, the expectation values of observables, and be able to write down the form of the wavefunction after a measurement.

    - to find the eigenstates and eigenenergies for specific time-independent Hamiltonians, and use these eigenstates to calculate expectation values and the probability of measurement outcomes.

    - to calculate the time dependence of the wavefunction and of expectation values as a function of time.

    - to write down the boundary conditions for the wavefunction for step-function 1-dimensional potentials. The student will be able to use these to calculate transmission and reflection coefficients.

    - to use the generic uncertainty principle to calculate uncertainties in observables.

    - to explain why wavefunctions and operators in quantum mechanics need to satisfy specific mathematical requirements.

    - to use Dirac notation to express quantum states and expectation values.

    - to calculate commutators of quantum mechanical operators.

    - Given a wavefunction for a quantum state expressed in one basis (for example, position basis), the student will be able to transform this into a wavefunction in a different basis (for example, momentum basis).

    - to calculate eigenfunctions, expectation values, and operators in vector and matrix representation.

In these notes our aim is to provide learning materials which are:

- self-contained
- easy to modify and remix, so we provide the full source, including the code
- open for reuse: see the license below.

Whether you are a student taking this course, or an instructor reusing the materials, we welcome all contributions, so check out the [course repository](https://gitlab.kwant-project.org/sconesaboj/qm1-notes), especially do [let us know](https://gitlab.kwant-project.org/sconesaboj/qm1-notes/issues/new?issuable_template=typo) if you see a typo!

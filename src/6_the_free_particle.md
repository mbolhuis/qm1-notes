---
title: The free particle
---

**Week 3, lecture 7**

# The free particle

You might have noticed that we haven't discussed so far the simplest quantum system, which corresponds to the case where the potential vanishes everywhere, $V(x)=0$ $\forall x$. This system is known as the _free particle_. As we will see in the following, this system is not as trivial as it might seem at first glance.

The time-independent Schrodinguer equation for the free particle reads


$$-\frac{\hbar^2}{2m}\frac{d^2\psi(x)}{dx^2}=E\psi(x),~~{\rm Eq.}~[1]$$

which can also be expressed as

$$\frac{d^2\psi(x)}{dx^2}=-\frac{2mE}{\hbar^2}\psi(x),~~{\rm Eq.}~[2]$$

which is the second-order differential equation that we have to solve. 

!!! info
    For homogeneous second order differential equations, the general solution is the linear combination of two particular independent solutions. To find a particular solution, we could try a possible ansatz and see if it works.  

You can convince yourselves that the most general solution of Eq.[2] is given by:

$$\psi(x)=Ae^{ikx}+Be^{-ikx},~~{\rm where}~~k\equiv\sqrt{\frac{2mE}{\hbar^2}}, ~~{\rm Eq.}~[3]$$

In contrast with the [infinite well case](https://qm1.quantumtinkerer.tudelft.nl/4_ISW/),  for the free particle system we don't have boundary conditions, and therefore the energy, $E$, can take any positive value (the only contribution to the total energy is the kinetic energy, which is positive-definite). As you can verify, in the limit $x\rightarrow \pm\infty$ the wavefunction $\psi$ does not vanish but rather oscillates around $0$. This feature has an important implication: the wavefunction corresponding to the free particle with fixed energy $E$ cannot be normalized and therefore cannot represent a physical quantum state.

!!! Recall
    One of the fundamental axioms of quantum mechanics is that the wave function of physical states admits a probabilistic interpretation. Therefore, a wave function that cannot be normalized cannot be associated to a physical quantum state.

The problem we are facing is originated by assuming that the free particle will have associated a _single value_ of the energy. This assumption leads to a wave function which is maximally delocalized: the particle has an equal probability to be found anywhere in space, including at infinity! You can verify this property by considering one of the particular solutions and trying to evaluate its normalization constant:

$$\psi(x)=Ae^{ikx},~~{\rm where}~~k=\sqrt{\frac{2mE}{\hbar^2}}$$,
$$\int_{-\infty}^{+\infty}\psi^*(x)\psi(x)dx=\int_{-\infty}^{+\infty}Ae^{-ikx}Ae^{ikx}dx=$$
$$\int_{-\infty}^{+\infty}A^2dx\rightarrow\infty$$

Therefore, there does not exist any value of $A$ for which the free-particle wave function with fixed energy can be normalised.

## The wave packet

To solve this problem, we are going to use the property that the eigenvectors $\psi_k(x)$ of the Schrodinguer equation of a free particle form a _complete basis_ and that therefore we can combine them to construct a physically valid wavefunction. That is, if the eigenvectors are given by $\psi_k(x)=Ae^{ikx},~~{\rm where}~~k=\sqrt{\frac{2mE}{\hbar^2}}$, then the most general wave function for this quantum system will be given by: 

$$\Psi(x)=\sum_k c_k \psi_k(x),~~{\rm Eq.~[4]}$$ 

for any complex values of the coefficients, $c_k$. Recall that this property is known as the **superposition principle**. In the case of a system like the free particle, where its solutions are not quantized (that is the wave numbere $k$ can take any value), we can replace the sum by an integral. Taking this into account, we construct the most general wavefunction for a free particle as

$$\Psi(x)=\frac{1}{\sqrt{2\pi}}\int_{-\infty}^{+\infty}e^{ikx}\phi(k)dk,~~{\rm Eq.}~[5]$$

In this expression, $\phi(k)$ is a general function of the wave number $k$ that corresponds to the continuum analog of the coefficients $c_k$ in the discrete case. Eq.~[5] is known as a **wave packet** since it is the linear combination of individual waves with well-defined frequency (that is, with fixed energy and wavelength).

What we need to do now is to determine for which functions $\phi(k)$ the wave function of the system Eq.~[5] will be normalizable and thus will admit the probabilistic inerpretation which is required for physical quantum systems. Note that even if the original wave functions $\psi_k(x)$ are not integrable, it is possible to combine them linearly such that their superposition is indeed integrable. 

By choosing specific values of the coefficients $\phi(k)$, one can make sure that the $\Psi(x)$ is correctly normalized even if it is constructued from the individual $\psi_k(x)=Ae^{ikx}$ wavefunctions which are not normalized. _How we can determine the coefficients $\phi(k)$?_ To achieve this, we will use the orthogonality property, which for quantized wave functions is given by
 
$$\int_{-\infty}^{+\infty}\psi_{l}^{*}(x)\psi_{n}(x)dx=\delta_{ln}$$

while for wave functions that are not quantized, as is the case for the free particle, the equivalent relation is given by

$$\int_{-\infty}^{+\infty}e^{ikx}e^{-ik'x}dx=2\pi\delta(k-k'),~~Eq.[6]$$

in other words, for wave functions which are not quantized the orthogonality relation is given in terms of the Dirac delta $\delta(k-k')$ instead of the Kronecker delta which is relevant for quantized wave functions.

Using this property we find that the coefficients $\phi(k)$ are given by

$$\phi(k)=\frac{1}{\sqrt{2\pi}}\int_{-\infty}^{+\infty}\Psi(x)e^{-ikx}dx,~~Eq.[7]$$

in other words $\phi(k)$ is the Fourier transform of $\Psi(x)$

!!! check "Example:"
    A free particle has the initial wave function
    $$\psi(x,0)=Ae^{-ax^2}$$
    where A and a are real and positive constants.

    (a) Normalize $\Psi(x,0)$.

    $$|A|^2\int_{-\infty}^{+\infty}e^{-2ax^2}dx=1$$
    $$|A|^2\sqrt{\frac{\pi}{2a}}=1~~\rightarrow~~|A|=(\frac{2a}{\pi})^{1/4}$$
    
    (b) Find $\Psi(x,t)$.
    
    $$\Psi(x,t)=\frac{1}{\sqrt{2\pi}}\int_{-\infty}^{+\infty}\phi(k)e^{i(kx-\omega t)}dk$$
    $$\phi(k)=\frac{1}{\sqrt{2\pi}}A\int_{-\infty}^{+\infty}e^{-ax^2}e^{-ikx}dx$$
    
    Hint: integrals of the form
    $$\int_{-\infty}^{+\infty}e^{-(ax^2+bx)}dx$$
    Let $y\equiv\sqrt{a}(x+(b/2a))$, and $(ax^2+bx)=y^2-(b^2/4a)$.
    $$\int_{-\infty}^{+\infty}e^{-(ax^2+bx)}dx=\int_{-\infty}^{+\infty}e^{-y^2+(b^2/4a)}\frac{1}{\sqrt{a}}dy=\frac{1}{\sqrt{a}}e^{b^2/4a}\int_{-\infty}^{+\infty}e^{-y^2}dy=\sqrt{\frac{\pi}{a}}e^{b^2/4a}$$
    
    $$\phi(k)=\frac{1}{\sqrt{2\pi}}A\int_{-\infty}^{+\infty}e^{-ax^2}e^{-ikx}dx=...=\frac{1}{(2\pi a)^{1/4}}e^{-k^2/4a}$$

    $$\Psi(x,t)=\frac{1}{\sqrt{2\pi}}\frac{1}{(2\pi a)^{1/4}}\int_{-\infty}^{+\infty}e^{-k^2/4a}e^{i(kx-(\frac{\hbar k^2}{2m})t)}dk=\frac{1}{\sqrt{2\pi}}\frac{1}{(2\pi a)^{1/4}}\int_{-\infty}^{+\infty}e^{-[(\frac{1}{4a}+i\frac{\hbar t}{2m})k^2-ixk]}dk$$

    $$\Psi(x,t)=(\frac{2a}{\pi})^{1/4}\frac{e^{-ax^2/(1+2i\hbar at/m)}}{\sqrt{1+2i\hbar at/m}}$$

    You can then evaluate the corresponding probability distribution $|\Psi(x,t)|^2$: you can verify that one ends up with a Gaussian distribution ceentered at $x=0$ and with variance increasing with time (see below). In other words, while $x=0$ remains at all times the most likely position of the particle, as time goes on, the probability of finding the particle far from the origin increases rapidly.

    <p align="center">
    ![image](figures/wavepacket.png)
    </p>

---
title: The uncertainty principle
---

**Week 4, lecture 12**

!!! summary "Learning objectives"

    After following this lecture you will be able:

    - To generalise Heisenberg's uncertainty principle to an arbitrary pair of observables.


# The uncertainty principle

You have already found before the Heisenberg's uncertainty principle:

$$\Delta x \Delta p \geq \frac{\hbar}{2}$$

Another similar relation that you should have been seen before is the Heisenberg's uncertainty principle for energy and time,

$$\Delta E \Delta t \geq \frac{\hbar}{2}$$

In this lecture, we aim to determine under which conditions these kind of relations hold for two general observables $A$ and $B$, thi is called **generalized uncertainty principle**.

To derive this, first of all we need to express the variance of a general operator. First of all, let's calculate the variance of the observable $A$ in the quantum state $|\psi \rangle$:

$$\sigma_A^2=<A^2>-<A>^2= <(A-<A>)^2>$$

>*Proof*:
    $<(A-<A>)^2> = <A^2+<A>^2-2A<A>>=<A^2>+<A>^2-2<A><A>=<A^2>-<A>^2$

We can also write the variance in the following form:

$$\sigma^2 = \langle (A-<A>)^2 \rangle= \langle ( A - <A>) \psi | ( A - <A>) \psi \rangle = \langle f| f \rangle$$

where $f \equiv ( A - <A>) \psi \rangle$.

In the same way, for a different observable $B$ its variance will be given by,

$$\sigma_B^2=\langle g | g \rangle$$

where we have defined the quantum state $g \equiv ( A - <A>) \psi \rangle$.

Now we are ready to derive the generalized uncertainty principle. 

For this we need to compute what is the product of the variances uncertainties associated to the operators $\hat{A}$ and $\hat{B}$

$$\sigma_A^2 \sigma_B^2=\langle f | f \rangle \langle g | g \rangle \ \geq |\langle f | g \rangle|^2$$ 

!!! note
    In the previous equation we have applied the **Schwarz inequality**. In general, for all vectors $u$ and $v$ of an inner product space it is true that: $|\langle u | v \rangle|^2 \leq \langle u | u \rangle \langle v | v \rangle$

!!! information
    For any complex number we have that
    $$|z|^2=[\Re (z)]^2+[\Im (z)]^2 \geq [\Im (z)]^2$$
    where $[\Im (z)]^2=[\frac{1}{2i} (z-z^*)]^2$

Therefore, letting $z=\langle f | g \rangle$

$$\sigma_A^2 \sigma_B^2 \geq |\langle f | g \rangle|^2$$

$$|\langle f | g \rangle|^2=\frac{1}{2i}(\langle f | g \rangle - \langle g | f \rangle)^2$$

therefore $$\sigma_A^2 \sigma_B^2 \geq =\frac{1}{2i}(\langle f | g \rangle - \langle g | f \rangle)^2$$

Now, the final step of our derivation is to evaluate the inner product $\langle f | g \rangle$. If you want to resproduce the derivation you can find it in Griffits, here we just quote the final results,

$$\langle f | g \rangle=\langle \hat{A} \hat{B} \rangle - \langle \hat{A} \rangle \langle \hat{B} \rangle$$
$$\langle g | f \rangle=\langle \hat{B} \hat{A} \rangle - \langle \hat{A} \rangle \langle \hat{B} \rangle$$

$$\sigma_A^2 \sigma_B^2 \geq \frac{1}{2i}([\langle \hat{A} \hat{B} \rangle - \langle \hat{A} \rangle \langle \hat{B} \rangle])^2$$

$$\sigma_A^2 \sigma_B^2 \geq \frac{1}{2i}(\langle [\hat{A}, \hat{B}] \rangle)^2$$

Let's discuss some important consequences of the generalized uncertainty principle:

1. If two physical observables commute $[\hat{A},\hat{B}]=0$ then $\sigma_A^2 \sigma_B^2 \geq 0$. That is we can measure both observables simultaneously with arbitrarialy good precision. In this case we say that A and B are observables are **compatible**.

2. If two physical observables do not commute $[\hat{A},\hat{B}] \neq 0$, we say that the observables are **incompatible**, they obey a Heisenberg uncertainty principle and you cannot measure them simultaneously with arbitrary precision.

## The free particle revisited

Let's connect this discussion with what we know about the free particle. The eigenfunctions of the Hamiltonian where $\psi_k(x)=Ae^{ikx}$, where $k\equiv \sqrt{\frac{2mE}{\hbar^2}}$.

What does the uncertainty principle tell us in this case?

Let us apply the linear momentum operator to this wavefunction

$$\hat{p} \psi_k(x)=\frac{\hbar}{i}\frac{\partial \psi_k(x)}{\partial x}=\frac{\hbar}{i} i k A e^{ikx}=\hbar k \psi_k(x)$$

we find that $\psi_k(x)$ is a determinate state of p (we always find the same result if we measure its linear momentum).

So, $\Delta p =0$ and the Heisenberg uncertainty principle $\Delta x \geq \frac{\hbar /2}{\Delta p} \rightarrow \infty$

If we construct a Gaussian wave packet $\psi_k(x)=Ae^{-ax^2}$ then it can be shown that $\Delta x \Delta p =\frac{\hbar}{2}$. Therefore, this means that a Gaussian wave packet has the minimum possible uncertainty.








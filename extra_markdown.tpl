{% extends 'markdown.tpl' %}

{%- block data_other -%}
{%- for type in output.data | filter_data_type -%}
{%- if type == 'application/vnd.plotly.v1+json' -%}
{%- set fname = output.metadata.filenames['application/vnd.plotly.v1+json'] -%}
{%- set plotly_url = fname | path2url -%}
{%- set div_id = fname.split('/') | last -%}
<div id="{{cookiecutter.div_id}}"><img ></img></div>

<script>
window.addEventListener('load', function() {
  Plotly.d3.json('../{{cookiecutter.plotly_url}}', function(error, fig) {
    Plotly.plot('{{cookiecutter.div_id}}', fig.data, fig.layout).then(
      function(value) {Plotly.relayout('{{cookiecutter.div_id}}', {height: ' '})}
    );
  });
});
</script>
{%- endif -%}
{%- endfor -%}
{%- endblock -%}

{% block error %}
{% endblock error %}

{% block stream %}
{%- if output.name == 'stdout' -%}
{{cookiecutter. output.text | indent }}
{%- endif -%}
{% endblock stream %}
